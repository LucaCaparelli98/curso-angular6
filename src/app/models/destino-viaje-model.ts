/*
export class DestinoViaje {
	nombre:String;
	imagenUrl:string;

	constructor(nombre:string, url:string) {
		this.nombre = nombre;
		this.imagenUrl = url;
	}
}
*/

export class DestinoViaje {
	private selected: boolean;
	public servicios: string[];

	constructor(public nombre: string, public url: string, public votes: number = 0){
		this.servicios = ['pileta', 'desayuno'];
	}

	isSelected(): boolean {
		return this.selected;
	}

	setSelected(s: boolean) {
		this.selected = s;
	}

	voteUp(): any {
		this.votes++;
	}

	voteDown(): any {
		this.votes--;
	}

}